var express =require('express');
var http = require('http');
var path = require('path');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var config = require('./config')
var userRoute = require('./routes/user.route')
var bcrypt = require('bcryptjs')

mongoose.connect(config.dbUrl);
mongoose.connection.on("connected", ()=>{
    console.log("Connected to mongo database");
})
var port = 3000
var app = express();

app.use(bodyParser.json());
app.use('/users', userRoute);

var server = http.createServer(app);

app.use(express.static(__dirname+ '/public'));

app.get('/', (req,res)=>{
    res.sendFile(path.join(__dirname, 'index.html'))
});

server.listen(port,()=>{
    console.log("server is starting - "+port);
});