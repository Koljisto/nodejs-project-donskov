var express = require("express");
const User = require("../models/user.model");
var router = express.Router();

router.post("/register", (req,res)=>{
    let user = new User({
        role:req.body.role,
        name:req.body.name,
        Sname:req.body.Sname,
        Tname:req.body.Tname,
        email:req.body.email,
        password:req.body.password
    });

    User.addUser(user, (err,result)=>{
        if(err){
            return res.json({success:false, message:err});
        }
        return res.json({success:true, message:result});
    })
});

router.post("/login", (req,res)=>{
    User.login(req.body.email, req.body.password, (err,result)=>{
        if(err){
            return res.json({success:false, message: err});
        }
        return res.json({success:true, message:result});
    })
});

module.exports = router;